# rdocufy

[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/sgtools%2Frdocufy?style=flat-square)](https://gitlab.com/sgtools/rdocufy/-/pipelines)
[![GitLab Release](https://img.shields.io/gitlab/v/release/sgtools%2Frdocufy?style=flat-square)](https://gitlab.com/sgtools/rdocufy/-/releases)
[![Static Badge](https://img.shields.io/badge/license-GPL%203.0-blue?style=flat-square)](https://gitlab.com/sgtools/rdocufy/-/blob/main/LICENSE)
[![Static Badge](https://img.shields.io/badge/Open%20Source%3F-Yes!-blue?style=flat-square)](#)

> Documentation helper for rust projects

**Main features**
 - To be used with helix editor

## Installation (Ubuntu Jammy 22.04)

```bash
sudo sh -c 'curl -SsL https://sgtools.gitlab.io/ppa/pgp-key.public | gpg --dearmor > /etc/apt/trusted.gpg.d/sgtools.gpg'
sudo sh -c 'curl -SsL -o /etc/apt/sources.list.d/sgtools.list https://sgtools.gitlab.io/ppa/sgtools.list'
sudo apt update
sudo apt install rdocufy
```

## Usage

```bash
rdocufy --help
```

- Use with helix editor
- Select a function (`maf` key shortcut)
- Exec `:pipe rdocufy`
- It will add a documentation template

See [documentation page](https://sgtools.gitlab.io/rdocufy)

## Dependencies

- 

## License

Copyright (C) 2024 Sebastien Guerri

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Contributors

- Sébastien Guerri: [@sguerri](https://gitlab.com/sguerri)
