## What's Changed in v0.1.2 
* feat: Add generic documentation line
* chore: Update TODO
* chore: Update code formatter

## What's Changed in v0.1.1 
* chore: Update config files
* chore: Update helix configuration

