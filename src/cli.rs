use clap::Command;

/// Creates CLI clap [Command][1]
///
/// [1]: https://docs.rs/clap/latest/clap/struct.Command.html
pub fn build_cli() -> Command
{
    Command::new(clap::crate_name!())
        .about(clap::crate_description!())
        .version(clap::crate_version!())
        .subcommand_required(false)
        .infer_subcommands(true)
        .allow_external_subcommands(false)
        .subcommand(
            Command::new("parse").about("Parse a single file"), // .arg(arg!(<PATH> "Path of file to parse").required(true)),
        )
        .subcommand(Command::new("clip").about("Clip selection for parsing"))
}
