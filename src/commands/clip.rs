use anyhow::Result;
use clap::ArgMatches;

/// Run command
pub fn run(_args: &ArgMatches) -> Result<()>
{
    let stdin = crate::utils::no_tty::stdin()?;

    if stdin.is_empty() {
        println!("//! <FILE DOCUMENTATION>")
    } else if let Some(doc) = crate::libs::parse::string(&stdin)? {
        println!("{}", doc);
    }

    println!("{}", stdin);
    Ok(())
}
