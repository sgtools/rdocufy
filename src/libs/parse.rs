use std::collections::HashMap;
use std::fmt::Debug;

use anyhow::Result;
use syn::punctuated::Punctuated;
use syn::token::Comma;
use syn::token::Plus;
use syn::Expr;
use syn::File;
use syn::ItemFn;
use syn::ReturnType;
use syn::Type;
use syn::TypeParamBound;

pub fn string<S>(content: S) -> Result<Option<String>>
where
    S: AsRef<str>,
{
    let file = syn::parse_file(content.as_ref())?;
    let doc = parse_function(&file)?;
    Ok(doc)
}

fn parse_function(file: &File) -> Result<Option<String>>
{
    if file
        .items
        .len()
        != 1
    {
        return Ok(None);
    }
    let item = file
        .items
        .first()
        .unwrap();
    if let syn::Item::Fn(item) = item {
        let doc = create_doc(item);
        Ok(Some(doc))
    } else {
        Ok(None)
    }
}

fn create_doc(func: &ItemFn) -> String
{
    let mut lines: Vec<String> = vec![];
    lines.push("/// <DESCRIPTION>".to_string());

    let mut args = vec![];
    for input in func
        .sig
        .inputs
        .iter()
    {
        let arg = match input {
            syn::FnArg::Receiver(_) => todo!(),
            syn::FnArg::Typed(pat_type) => {
                let arg_name = if let syn::Pat::Ident(ident) = &pat_type
                    .pat
                    .as_ref()
                {
                    ident
                        .ident
                        .to_string()
                } else {
                    todo!()
                };
                let arg_type = handle_type(
                    pat_type
                        .ty
                        .as_ref(),
                );
                (arg_name, arg_type)
            }
        };
        args.push(arg);
    }
    if !args.is_empty() {
        lines.push("///".to_string());
        lines.push("/// # Parameters".to_string());
        lines.push("///".to_string());
        lines.extend(
            args.iter()
                .map(|(name, ty)| {
                    format!(
                        "/// - `{}`: A {} that <DESCRIPTION>",
                        name,
                        ty.to_description()
                    )
                }), // .fold(String::new(), |p, c| p + &c),
        )
    }

    if let syn::ReturnType::Type(_, return_type) = &func
        .sig
        .output
    {
        lines.push("///".to_string());
        lines.push("/// # Returns".to_string());
        lines.push("///".to_string());
        lines.push(format!(
            "/// A {} which is:",
            handle_type(return_type.as_ref()).to_description()
        ));
        // let a = handle_type(return_type.as_ref());
        // println!("{}", a.name);

        lines.push("///".to_string());
        lines.push("/// - `<VALUE>` if <DESCRIPTION>".to_string());
    }

    let mut where_clauses = HashMap::<String, Vec<String>>::new();

    if let Some(where_clause) = &func
        .sig
        .generics
        .where_clause
    {
        where_clause
            .predicates
            .pairs()
            .map(|p| p.into_value())
            .for_each(|p| {
                match p {
                    syn::WherePredicate::Lifetime(_) => { // SKIP 'a:
                    }
                    syn::WherePredicate::Type(t) => {
                        let bounded = handle_type(&t.bounded_ty).name;
                        let bounds = handle_bounds(&t.bounds);
                        if let Some(w) = where_clauses.get_mut(&bounded) {
                            w.push(bounds);
                        } else {
                            where_clauses.insert(bounded, vec![bounds]);
                        }
                        // t.lifetimes
                    }
                    _ => unreachable!(),
                }
            });
    }

    let mut type_generics = vec![];
    func.sig
        .generics
        .clone()
        .type_params()
        .for_each(|d| {
            let name = d
                .ident
                .to_string();

            let mut bounds = match handle_bounds(&d.bounds) {
                s if s.is_empty() => vec![],
                s => vec![s],
            };
            if let Some(external_bounds) = where_clauses.get_mut(&name) {
                bounds.append(external_bounds);
            }
            let bounds = bounds.join(" + ");

            let default = d
                .default
                .clone()
                .map(|d| handle_type(&d))
                .map(|t| t.to_string());
            // .map(|s| format!(" = {}", s));

            type_generics.push(MyGenerics {
                name,
                bounds,
                default,
            });
        });

    let mut const_generics = vec![];
    func.sig
        .generics
        .clone()
        .const_params()
        .for_each(|p| {
            // p.attrs
            let name = p
                .ident
                .to_string();
            let bounds = handle_type(&p.ty).to_string();
            let default = p
                .default
                .clone()
                .map(|d| handle_expr(&d));
            // .map(|s| format!(" = {}", s));
            const_generics.push(MyGenerics {
                name,
                bounds,
                default,
            })
        });

    if !type_generics.is_empty() || !const_generics.is_empty() {
        lines.push("///".to_string());
        lines.push("/// # Type parameters".to_string());
        lines.push("///".to_string());
        for g in type_generics.iter() {
            lines.push(g.to_string());
        }
        for g in const_generics.iter() {
            lines.push(g.to_string());
        }
    }

    let mut lifetimes = vec![];
    func.sig
        .generics
        .clone()
        .lifetimes()
        .for_each(|l| {
            lifetimes.push(
                l.lifetime
                    .ident
                    .to_string(),
            );
        });

    if !lifetimes.is_empty() {
        lines.push("///".to_string());
        lines.push("/// # Lifetimes".to_string());
        lines.push("///".to_string());
        for l in lifetimes.iter() {
            lines.push(format!("/// - `{}`: The life time of <DESCRIPTION>", l));
        }
    }

    lines.push("///".to_string());
    lines.push("/// # Examples".to_string());
    lines.push("///".to_string());
    lines.push("/// ```rust".to_string());
    lines.push("/// ```".to_string());

    lines.join("\n")
}

fn handle_type(t: &Type) -> MyType
{
    match t {
        Type::Array(value) => {
            let name = handle_type(
                value
                    .elem
                    .as_ref(),
            )
            .to_string();
            let len = handle_expr(&value.len);
            let name = format!("[{}; {}]", name, len);
            MyType::new(name)
        }
        Type::BareFn(_) => MyType::new("<UNKNOWN BAREFN>"),
        Type::Group(_) => MyType::new("<UNKNOWN GROUP>"),
        Type::ImplTrait(value) => {
            let name = handle_bounds(&value.bounds);
            let name = format!("impl {}", name);
            MyType::new(name)
        }
        Type::Infer(_) => MyType::new("<UNKNOWN INFER>"),
        Type::Macro(_) => MyType::new("<UNKNOWN MACRO>"),
        Type::Never(_) => MyType::new("<UNKNOWN NEVER>"),
        Type::Paren(value) => handle_type(
            value
                .elem
                .as_ref(),
        ),
        Type::Path(value) => {
            if let Some(ident) = value
                .path
                .get_ident()
            {
                // MyType::new(format!("[`{}`]", ident))
                MyType::new(ident.to_string())
            } else {
                handle_path(&value.path)
            }
        }
        Type::Ptr(value) => {
            let is_mut = value
                .mutability
                .is_some();
            let is_const = value
                .const_token
                .is_some();
            handle_type(
                value
                    .elem
                    .as_ref(),
            )
            .pointer()
            .with_mutable(is_mut)
            .with_const(is_const)
        }
        Type::Reference(value) => {
            let is_mut = value
                .mutability
                .is_some();
            handle_type(
                value
                    .elem
                    .as_ref(),
            )
            .reference()
            .with_mutable(is_mut)
        }
        Type::Slice(value) => {
            let name = handle_type(
                value
                    .elem
                    .as_ref(),
            );
            MyType::new(format!("[{}]", name)).slice()
            // MyType::new(format!("[{}]", name.name)).slice()
        }
        Type::TraitObject(value) => {
            let is_dyn = value
                .dyn_token
                .is_some();
            let name = handle_bounds(&value.bounds);
            let name = format!("{}{}", if is_dyn { "dyn " } else { "" }, name);
            MyType::new(name)
        }
        Type::Tuple(value) => {
            let names = value
                .elems
                .pairs()
                .map(|p| p.into_tuple())
                .fold(String::new(), |prev, (cur, sep)| {
                    prev + &handle_type(cur).to_string() + sep.map_or("", |_| ", ")
                });
            MyType::new(format!("({})", names))
        }
        Type::Verbatim(_) => MyType::new("<UNKNOWN VERBATIM>"),
        _ => MyType::new("<UNKNOWN>"),
    }
}
// {{name}} - {{description}}
// Copyright (C) {{year}} Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

// use crate::utils::param;
// use crate::Context;

// TODO: Move to sgtools_libs
// fn read_dir_loop<P>(path: P, entries: &mut Vec<PathBuf>) -> Result<()>
// where
//     P: AsRef<Path>,
// {
//     for entry in fs::read_dir(path)? {
//         let entry = entry?;
//         let path = entry.path();
//         if path.is_file() {
//             entries.push(path)
//         } else if path.is_dir() {
//             read_dir_loop(path, entries)?;
//         }
//     }
//     Ok(())
// }

// // TODO: Move to sgtools_libs
// fn read_dir<P>(path: P) -> Result<Vec<PathBuf>>
// where
//     P: AsRef<Path>,
// {
//     let mut entries = vec![];
//     read_dir_loop(path, &mut entries)?;
//     Ok(entries)
// }

// fn get_usetree(tree: &UseTree, prev: &mut Vec<String>, result: &mut HashMap<String, String>) {
//     match tree {
//         UseTree::Path(path) => {
//             let k = path.ident.to_string();
//             prev.push(k);
//             get_usetree(&path.tree, prev, result);
//         }
//         UseTree::Name(name) => {
//             let k = name.ident.to_string();
//             prev.push(k.clone());
//             result.insert(k, prev.join("::"));
//         }
//         UseTree::Rename(rename) => {
//             let r = rename.ident.to_string();
//             let k = rename.rename.to_string();
//             prev.push(r);
//             result.insert(k, prev.join("::"));
//         }
//         UseTree::Glob(_) => { /* NOT HANDLED */ }
//         UseTree::Group(group) => {
//             for tree in group.items.pairs().map(|p| p.into_value()) {
//                 get_usetree(tree, &mut prev.clone(), result);
//             }
//         }
//     }
// }

// fn parse_declarations(file: &File) -> HashMap<String, String> {
//     let mut declarations = HashMap::<String, String>::new();
//     for item in file.items.iter() {
//         if let syn::Item::Use(item) = item {
//             get_usetree(&item.tree, &mut vec![], &mut declarations);
//         }
//     }
//     declarations
// }

// fn parse_functions(file: &File) -> HashMap<String, String> {
//     let mut functions = HashMap::<String, String>::new();
//     for item in file.items.iter() {
//         if let syn::Item::Fn(func) = item {
//             let name = &func.sig.ident.to_string();
//             let has_doc = has_doc(func);
//             if !has_doc {
//                 let doc = create_doc(func);
//                 functions.insert(name.clone(), doc);
//                 // println!("{}:\n{}", name, doc);
//             } else {
//                 // println!("{:#?}", item);
//             }
//         }
//     }
//     functions
// }

// fn parse_file<P>(path: P) -> Result<()>
// where
//     P: AsRef<Path> + Debug,
// {
//     println!("PATH: {:?}", path);

//     let content = std::fs::read_to_string(path)?;
//     let file = syn::parse_file(&content)?;
//     // let declarations = parse_declarations(&file);
//     let functions = parse_functions(&file);

//     println!("SPAN {:?}", file.span().source_text());

//     println!("{:#?}", functions);

//     Ok(())
// }

/// Run command
// pub fn run(_args: &ArgMatches, _context: &Context) -> Result<()> {
//     // println!("{:#?}", context);

//     // let json = param::get_bool(args, "json");
//     // let no_tty = param::get_bool(args, "no-tty");
//     // let verbose = param::get_bool(args, "verbose");
//     // let color = param::get_str(args, "color", "auto")?;
//     // let path = param::get_string(args, "PATH", "")?;
//     let path = "test";

//     // println!("JSON    : {}", json);
//     // println!("NO_TTY  : {}", no_tty);
//     // println!("VERBOSE : {}", verbose);
//     // println!("COLOR   : {}", color);
//     // println!("PATH    : {}", path);

//     for path in read_dir(path)?.iter() {
//         if !path.extension().is_some_and(|e| e.eq("rs")) {
//             continue;
//         }
//         parse_file(path)?;
//     }

//     // let path = Path::new(&path);
//     // if !path.exists() {
//     //     return Err(anyhow!("Path does not exist"));
//     // }
//     // if !path.is_file() {
//     //     return Err(anyhow!("Path is not a file"));
//     // }
//     // if !path.extension().is_some_and(|e| e.eq("rs")) {
//     //     return Err(anyhow!("Path is not a rust source file"));
//     // }

//     // return Ok(());

//     // let content = std::fs::read_to_string(path)?;
//     // let parsed = syn::parse_file(&content)?;

//     // for item in parsed.items.iter() {
//     //     if let syn::Item::Fn(func) = item {
//     //         // println!("{:#?}", func);
//     //         let name = &func.sig.ident.to_string();
//     //         let has_doc = has_doc(func);
//     //         if !has_doc {
//     //             // println!("{:#?}", func);
//     //             let doc = create_doc(func);
//     //             println!("{}:\n{}", name, doc);
//     //         } else {
//     //             println!("{:#?}", item);
//     //         }
//     //         // println!("{}: {}", name, has_doc);
//     //     }
//     // }

//     Ok(())
// }

// fn has_doc(func: &ItemFn) -> bool {
//     let mut result = false;
//     for attr in func.attrs.iter() {
//         if let Meta::NameValue(name) = &attr.meta {
//             if name.path.is_ident("doc") {
//                 result = true;
//             }
//         }
//     }
//     result
// }

#[derive(Debug)]
struct MyGenerics
{
    name: String,
    bounds: String,
    default: Option<String>,
}

impl std::fmt::Display for MyGenerics
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        let bounded = if self
            .bounds
            .is_empty()
        {
            "".to_string()
        } else {
            format!(" bounded to `{}`", self.bounds)
        };
        let defaulted = if let Some(default) = &self.default {
            format!(" (defaulted to `{}`)", default)
        } else {
            "".to_string()
        };
        let str = format!(
            "/// - `{}`: A type{}{} that <DESCRIPTION>",
            self.name, bounded, defaulted
        );
        f.write_str(&str)
    }
}

// struct MyConstGenerics {
//     name: String,
//     default: Option<String>,
// }

fn handle_inputs(inputs: &Punctuated<Type, Comma>) -> String
{
    inputs
        .pairs()
        .map(|p| p.into_tuple())
        .fold(String::new(), |prev, (cur, sep)| {
            prev + &handle_type(cur).to_string() + sep.map_or("", |_| ", ")
        })
}

fn handle_output(output: &ReturnType) -> String
{
    match output {
        ReturnType::Default => "".to_string(),
        ReturnType::Type(_, t) => {
            format!(" -> {}", handle_type(t.as_ref()))
        }
    }
}

fn handle_path(p: &syn::Path) -> MyType
{
    if let Some(ident) = p.get_ident() {
        MyType::new(format!("[`{}`]", ident))
        // MyType::new(ident.to_string())
    } else {
        // println!("{:#?}", p);
        let (name, args) = p
            .segments
            .pairs()
            .map(|p| p.into_tuple())
            .fold(
                (String::new(), String::new()),
                |(prev_name, _), (s, sep)| {
                    let mut name = prev_name
                        + &s.ident
                            .to_string();
                    let args = match &s.arguments {
                        syn::PathArguments::None => "".to_string(),
                        syn::PathArguments::AngleBracketed(p_args) => {
                            let args = p_args
                                .args
                                .pairs()
                                .map(|p| p.into_tuple())
                                .fold(String::new(), |prev, (cur, sep)| {
                                    let n = match cur {
                                        syn::GenericArgument::Lifetime(_) => todo!(),
                                        syn::GenericArgument::Type(t) => handle_type(t),
                                        syn::GenericArgument::Const(_) => todo!(),
                                        syn::GenericArgument::AssocType(_) => todo!(),
                                        syn::GenericArgument::AssocConst(_) => todo!(),
                                        syn::GenericArgument::Constraint(_) => todo!(),
                                        _ => todo!(),
                                    };
                                    prev + &n.to_string() + sep.map_or("", |_| ", ")
                                });
                            args
                            // format!("<{}>", args)
                        }
                        syn::PathArguments::Parenthesized(p_args) => {
                            let inputs = handle_inputs(&p_args.inputs);
                            let output = handle_output(&p_args.output);
                            // format!("({}){}", inputs, output)
                            name += &format!("({}){}", inputs, output);
                            "".to_string()
                        }
                    };
                    if sep.is_some() {
                        name += "::";
                    }
                    (name, args)
                    // if sep.is_some() {
                    //     n + &np + "::"
                    // } else {
                    //     n + &np
                    // }
                },
            );
        // MyType::new(format!("[`{}`]", name)).with_params(args)
        MyType::new(name).with_params(args)
    }
}

#[derive(Debug)]
struct MyType
{
    name: String,
    params: Option<String>,
    is_reference: bool,
    is_pointer: bool,
    is_mutable: bool,
    is_const: bool,
    is_slice: bool,
}

impl MyType
{
    pub fn new<S: AsRef<str>>(name: S) -> Self
    {
        Self {
            name: name
                .as_ref()
                .to_string(),
            params: None,
            is_reference: false,
            is_pointer: false,
            is_mutable: false,
            is_const: false,
            is_slice: false,
        }
    }

    #[allow(dead_code)]
    pub fn with_params<S>(mut self, params: S) -> Self
    where
        S: AsRef<str>,
    {
        if !params
            .as_ref()
            .is_empty()
        {
            self.params = Some(
                params
                    .as_ref()
                    .to_string(),
            );
        }
        self
    }

    #[allow(dead_code)]
    pub fn reference(mut self) -> Self
    {
        self.is_reference = true;
        self
    }

    #[allow(dead_code)]
    pub fn with_reference(mut self, value: bool) -> Self
    {
        self.is_reference = value;
        self
    }

    #[allow(dead_code)]
    pub fn pointer(mut self) -> Self
    {
        self.is_pointer = true;
        self
    }

    #[allow(dead_code)]
    pub fn with_pointer(mut self, value: bool) -> Self
    {
        self.is_pointer = value;
        self
    }

    #[allow(dead_code)]
    pub fn mutable(mut self) -> Self
    {
        self.is_mutable = true;
        self
    }

    #[allow(dead_code)]
    pub fn with_mutable(mut self, value: bool) -> Self
    {
        self.is_mutable = value;
        self
    }

    #[allow(dead_code)]
    pub fn r#const(mut self) -> Self
    {
        self.is_const = true;
        self
    }

    #[allow(dead_code)]
    pub fn with_const(mut self, value: bool) -> Self
    {
        self.is_const = value;
        self
    }

    #[allow(dead_code)]
    pub fn slice(mut self) -> Self
    {
        self.is_slice = true;
        self
    }

    #[allow(dead_code)]
    pub fn with_slice(mut self, value: bool) -> Self
    {
        self.is_slice = value;
        self
    }

    // pub fn to_string(&self, short: bool) -> String {
    //     if short {
    //         format!(
    //             "{}{}{}{}{}",
    //             if self.is_reference { "&" } else { "" },
    //             if self.is_pointer { "*" } else { "" },
    //             if self.is_const { "const " } else { "" },
    //             if self.is_mutable { "mut " } else { "" },
    //             self.name
    //         )
    //     } else {
    //         format!(
    //             "{}{}`{}`{}{}",
    //             if self.is_mutable { "mutable " } else { "" },
    //             if self.is_const { "const " } else { "" },
    //             self.name,
    //             if self.is_reference {
    //                 if self.is_slice {
    //                     " slice"
    //                 } else {
    //                     " reference"
    //                 }
    //             } else {
    //                 ""
    //             },
    //             if self.is_pointer { " pointer" } else { "" }
    //         )
    //     }
    // }

    pub fn to_description(&self) -> String
    {
        // let mut name = self.name.clone();
        // if let Some(declaration) = declarations.get(&name) {
        //     name = format!("[{}][{}]", name, declaration);
        // }

        format!(
            "{}{}`{}{}`{}{}",
            // "{}{}[`{}`]{}{}{}",
            // "{}{}{}{}{}{}",
            if self.is_mutable { "mutable " } else { "" },
            if self.is_const { "const " } else { "" },
            self.name,
            self.params
                .clone()
                .map_or("".to_string(), |p| format!("<{}>", p)),
            // .map_or("".to_string(), |p| format!("<[`{}`]>", p)),
            if self.is_reference {
                if self.is_slice {
                    " slice"
                } else {
                    " reference"
                }
            } else {
                ""
            },
            if self.is_pointer { " pointer" } else { "" }
        )
    }
}

impl std::fmt::Display for MyType
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        let content = format!(
            "{}{}{}{}{}{}",
            if self.is_reference { "&" } else { "" },
            if self.is_pointer { "*" } else { "" },
            if self.is_const { "const " } else { "" },
            if self.is_mutable { "mut " } else { "" },
            self.name,
            self.params
                .clone()
                .map_or("".to_string(), |p| format!("<{}>", p))
        );
        f.write_str(&content)
    }
}

fn handle_bounds(bounds: &Punctuated<TypeParamBound, Plus>) -> String
{
    bounds
        .pairs()
        .map(|p| p.into_tuple())
        .fold(String::new(), |prev, (cur, sep)| {
            let name = match cur {
                syn::TypeParamBound::Trait(v) => {
                    let modifier = match v.modifier {
                        syn::TraitBoundModifier::None => "",
                        syn::TraitBoundModifier::Maybe(_) => "?",
                    };
                    format!("{}{}", modifier, handle_path(&v.path).name)
                }
                syn::TypeParamBound::Lifetime(v) => format!("'{}", v.ident),
                syn::TypeParamBound::Verbatim(_) => todo!(),
                _ => todo!(),
            };
            prev + &name + sep.map_or("", |_| " + ")
        })
}

fn handle_expr(e: &Expr) -> String
{
    if let syn::Expr::Lit(v) = e {
        match &v.lit {
            syn::Lit::Str(v) => v.value(),
            syn::Lit::ByteStr(v) => v
                .token()
                .to_string(),
            syn::Lit::CStr(v) => v
                .value()
                .into_string()
                .unwrap_or("<UNKNOWN>".to_string()),
            syn::Lit::Byte(v) => format!("{}", v.value()),
            syn::Lit::Char(v) => v
                .value()
                .to_string(),
            syn::Lit::Int(v) => v.to_string(),
            syn::Lit::Float(v) => v.to_string(),
            syn::Lit::Bool(v) => format!("{}", v.value()),
            syn::Lit::Verbatim(_) => "<UNKNOWN>".to_string(),
            _ => "<UNKNOWN>".to_string(),
        }
    } else if let syn::Expr::Path(v) = e {
        handle_path(&v.path).to_string()
    } else {
        "<UNKNOWN>".to_string()
    }
}

// Common headlines

// These are the common headlines from RFC 1574:

// Examples
//     Must contain at least one Rust code snippet
//     Should use sub-headlines when multiple examples are given. Each sub-section should have a headline, followed by a short paragraph explaining the examples, and at least one Rust code block.
// Panics
//     Explains when a function panics, should always be included when panic!, assert! or similar are used/when any branch of the function can directly return !
// Errors
//     Explain when an error value is returned (see also “Returns” in the next section)
// Safety
//     Describes the safety requirements of this function
// Aborts
//     Similar to panic
// Undefined Behavior
//     Describes for which inputs the function behavior is not defined
// Platform-specific behavior
//     Describe different behavior of function or module depending on the target platform
// See also
//     List of links to other pages that are relevant to this one
//     Might make use of intra doc links to refer to other Rust items

// Specific machine readable sections

// This is aimed at documenting functions and methods, but should also work for documenting type parameters and lifetime parameters of structs, enums, and traits.

// Parameters
//     (always plural)
//     List of parameter names with description
// Returns
//     (read either as 3rd person singular form or plural “return [values]”)
//     Plain text description
//     can be followed by a list of valid enum variants for the return type and descriptions
//     Alternatively, allow regular pattern matching syntax instead of enum variant names
// Type parameters
//     List of generic type parameters (the T in fn foo<T>()) and description (can also be used to describe trait bounds in where clauses)
// Lifetimes
//     (alternatively “Lifetime parameters”)
//     List of valid lifetime identifiers (without leading ') and description
