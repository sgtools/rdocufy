use anyhow::Result;
use sgtools_libs as utils;

mod cli;
mod commands;
mod libs;

fn main() -> Result<()>
{
    let matches = cli::build_cli().get_matches();

    match matches.subcommand() {
        Some(("parse", args)) => commands::parse::run(args)?,
        Some(("clip", args)) => commands::clip::run(args)?,
        Some((_, _)) => unreachable!(),
        None => commands::clip::run(&matches)?,
    };

    Ok(())
}
